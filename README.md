# ChatbotWidget

Javascript script that integrates a chatbot widget to websites.

The widget communicates with the chat bot api via a web socket.

# Setup

install node / npm

npm i

# Development

npm run develop

# Build

npm run build

# Deployment Link

https://dicu-webwidget.netlify.app/

# TODO

- Move to react
