const path = require('path');

module.exports = {
  entry: {
    main: ['./js/script.js'],
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, './dist'),
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include:path.resolve(__dirname,"js"),
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test:/\.css$/,
        include:path.resolve(__dirname,"css"),
        use:["style-loader","css-loader"]
    }
    ],
  },
};


