import { getCookie } from './helper';

let session;
let socket;

export async function setupWebsocket(callbackOnMessage) {
  console.log('Checking for dicu-session cookie...');
  session = getCookie('dicu-session');
  console.log('Current dicu-session cookie:', session);

  const websockedUrl = 'ws://3.67.196.199:8070/api/messages';
  socket = new WebSocket(websockedUrl);

  // Connection opened callback
  socket.addEventListener('open', function (event) {
    console.log('Received open event:', event);
    console.log('Sending start event with session:', session);
    socket.send(JSON.stringify({ event: 'start', session: session }));
  });

  socket.addEventListener('close', (event) => {
    console.log('The connection has been closed successfully.');
  });

  // Listen for incomming events
  socket.addEventListener('message', function (event) {
    let data = JSON.parse(event.data);
    if (data.event == 'new_message') {
      console.log(data);
      callbackOnMessage(data);
    } else if (data.event == 'update_cookie') {
      console.log('Recieved cookie update event...');
      session = data.session;
      document.cookie = `dicu-session=${session}`;
      console.log('Setting session cookie to', session);
    }
  });
}

// Sends messages to the socket
export function sendMessage(text) {
  socket.send(
    JSON.stringify({ event: 'new_message', message: { text: text } })
  );
}
