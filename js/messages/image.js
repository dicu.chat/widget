import { numberToPixel } from '../services/helper';
import Component from '../component';
import Text from './text';

class Image extends Component {
  constructor(parent, src, isUser) {
    super(parent);
    this.isUser = isUser;
    this.src = src;
    if (this.isUser) {
      this.backgroundColor = '#42a5f5';
      this.textColor = '#ffffff';
    } else {
      this.backgroundColor = '#f7f7f7';
      this.textColor = '#6c6c6c';
    }

    this.style = {
      margin: numberToPixel(5),
      marginBottom: numberToPixel(20),
      overflow: 'hidden',
      display: 'block',
      maxWidth: numberToPixel(330),
      width: 'auto',
      height: 'auto',
    };

    this.render();
  }

  create() {
    this.element = document.createElement('img');
    this.element.src = this.src;
  }
}

export default Image;
